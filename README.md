### Setup/Installation
* Clone/download the repo/folder to a location of your choice
* Open the terminal (Mac)/command-prompt (Windows) and type the following command:
```shell
pip3 install -r requirements.txt
```
* If above command fails try:
```shell
pip install -r requirements.txt
```
* If you use anaconda--add anaconda to system PATH or open anaconda-prompt and try:
```shell
conda install -r requirements.txt
```

### Learning Objectives
* Learn web development in **pure** Python!
* Understand standard backend concepts such as session, cache etc
* Generic & Semantic Data Profiling
* Hosting/Logging on AWS

### Running the App
* Make the project directory as your "current directory" (i.e. open the folder in an IDE/terminal)
* Use the following command to run the app:--
```shell
streamlit run data_profiler.py
```

### Workflow
* The landing page has a **file uploader form**
* User uploads CSV to website
* App generates **Generic Profile** using pandas-profiling (read more here: [https://pandas-profiling.github.io/pandas-profiling/docs/master/rtd/pages/getting_started.html](https://pandas-profiling.github.io/pandas-profiling/docs/master/rtd/pages/getting_started.html))
* Post profiling--user is asked to select a **categorical** column, a similarity measure:--cosine, levenshtein, trigrams etc & no. of clusters--and use these parameters to cluster duplicates in that column
* Show cluster results through heatmap & dendrograms
* Provide a downloadable file which contains the cluster results (which on the backend is logged on S3 & a pre-signed URL is generated for download)

### Semantic Profiling
#### Problem:--
* Suppose you've this column:--  

| **City** | 
| --------------- |
| chicago |
| illinois, chicago |
| Chiccaaa~go |
| nyc |
| new york, New York |
| nyc, the hood! |

* And you want to **cluster duplicates** so results should be:---

| **City** | **cluster_num** |
| --------------- | --------------- |
| chicago | 0 |
| illinois, chicago | 0 |
| Chiccaaa~go | 0 |
| nyc | 1 |
| new york, New York | 1 |
| nyc, the hood! | 1 |

