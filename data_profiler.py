import numpy as np
import pandas as pd
from pandas_profiling import ProfileReport

from sklearn.cluster import AgglomerativeClustering
from fuzzy_match import algorithims as A
from fuzzywuzzy import fuzz
import re

import plotly.figure_factory as ff
import plotly.express as px

import streamlit as st
from streamlit_pandas_profiling import st_profile_report
from streamlit import session_state as session

import boto3
from io import StringIO


def val_wrangler(x, discard_non_alphanumeric=False, discard_numeric=False):
    if discard_non_alphanumeric==True:
        x = re.sub('[^0-9a-zA-Z]+', ' ', x)
    if discard_numeric==True:
        x = re.sub(r'[0-9]+', '', x)
    x = x.replace(",", " ").replace("/", " ").replace("|", " ").replace("&", " ").replace("_", " ").replace("'","").replace(";","")
    x = x.strip()
    x = re.sub('\s+',' ',x)
    words = x.split(" ")
    final_words=[]
    for i,word in enumerate(words):
        final_words.extend(re.findall(r'[0-9|A-Z|a-z](?:[.|0-9|a-z]+|[A-Z]*(?=[A-Z]|$))', word))
    final_words = [w.lower() for w in final_words]
    final_words = [w.encode("utf-8").decode() for w in final_words]
    return " ".join(final_words)

@st.cache(suppress_st_warning=True)
def get_similarity_matrix(val_dict, measure):
    with st.spinner(text="Creating Similarity Matrix"):
        similarity_matrix = []
        for val, clean_val in val_dict.items():
            row=[]
            for val1, clean_val1 in val_dict.items():
                if measure=="cosine":
                    element = A.cosine(clean_val, clean_val1)
                if measure=="levenshtein":
                    element = A.levenshtein(clean_val, clean_val1)
                if measure=="jaro_winkler":
                    element = A.jaro_winkler(clean_val, clean_val1)
                if measure=="trigram":
                    element = A.trigram(clean_val, clean_val1)
                if measure=="levenshtein_partial":
                    element = fuzz.partial_ratio(clean_val, clean_val1)
                row.append(element)
            similarity_matrix.append(row)
    st.success("Success")
    return pd.DataFrame(data=np.asarray(similarity_matrix), index=val_dict.keys(), columns=val_dict.keys())

def get_viz(sim_df):
    heatmap = px.imshow(sim_df)
    heatmap.update_layout(width=1300, height=1200)
    st.plotly_chart(heatmap)

    dendrogram = ff.create_dendrogram(sim_df, labels=sim_df.index)
    dendrogram.update_layout(width=1300, height=1200)
    st.plotly_chart(dendrogram)

def get_cluster_labels(sim_df, n_clusters, aff, val_dict):
    methods = ["ward", "complete", "average", "single"]
    ward = AgglomerativeClustering(n_clusters=n_clusters, affinity="euclidean", linkage=methods[0]).fit(sim_df)
    complete = AgglomerativeClustering(n_clusters=n_clusters, affinity=aff, linkage=methods[1]).fit(sim_df)
    avg = AgglomerativeClustering(n_clusters=n_clusters, affinity=aff, linkage=methods[2]).fit(sim_df)
    single = AgglomerativeClustering(n_clusters=n_clusters, affinity=aff, linkage=methods[3]).fit(sim_df)

    res = list(zip(sim_df.index, ward.labels_, complete.labels_, avg.labels_, single.labels_))
    res_df = pd.DataFrame.from_records(res, columns=['name', 'ward', 'complete', 'average', 'single'])
    res_df["clean_name"] = res_df['name'].apply(lambda val: val_dict[val])
    res_df = res_df[['name', 'clean_name', 'ward', 'complete', 'average', 'single']].copy()
    res_df.sort_values(by=['ward'], inplace=True)
    return res_df

def save_to_S3(cluster_df, fname):
    st.write(cluster_df)
    csv_buffer = StringIO()
    cluster_df.to_csv(csv_buffer, sep="\t", line_terminator="\n", header=True, index=False)
    s3_resource = boto3.resource('s3')
    s3 = boto3.client('s3')
    res_fname = 'clusters_' + fname.split('.')[0] + ".csv"
    s3_resource.Object('streamlit-poc-dump', res_fname).put(Body=csv_buffer.getvalue())
    share_url = s3.generate_presigned_url(ClientMethod='get_object',
                                          ExpiresIn=3600,
                                          Params={'Bucket': 'streamlit-poc-dump', 'Key': res_fname})
    st.markdown("**[Download cluster file]({})**".format(share_url))


def cluster_duplicates(df, filename):
    col, dis_num, dis_special, sim, aff = session.col_name, session.dis_num, session.dis_non_alphanum, session.similarity, session.affinity
    n_clusters = session.num_clusters

    df[col] = df[[col]].fillna("N/A")
    values = sorted(df[col].unique())
    clean_vals = {val:val_wrangler(val, dis_special, dis_num) for val in values}
    sim_matrix = get_similarity_matrix(clean_vals, sim)
    get_viz(sim_matrix)
    cluster_df = get_cluster_labels(sim_matrix, n_clusters, aff, clean_vals)
    save_to_S3(cluster_df, filename)

def profiler():
    df = pd.read_csv(session.upload, sep=session.delim, engine="python")
    filename = session.upload.name
    ftype = session.upload.type
    file_info = {"Filename": filename, "FileType": ftype}
    st.write(file_info)
    st.title("{} Profile".format(file_info["Filename"]))
    pr = ProfileReport(df, explorative=True)
    st_profile_report(pr)

    with st.form(key="cluster_duplicates"):
        cols = list(df.columns)
        col_name = st.selectbox('Enter Column Name', cols, key="col_name")#returns str
        dis_num = st.checkbox("discard_numeric", key="dis_num")#returns boolean
        dis_non_alphanum = st.checkbox("discard_non_alphanumeric", key="dis_non_alphanum")# returns boolean
        similarity = st.radio(label="Select Similarity Measure", options=["levenshtein", "cosine", "jaro_winkler", "trigram", "levenshtein_partial"], key="similarity")
        affinity = st.radio(label="Select Distance Measure", options=["euclidean", "l1", "l2", "manhattan", "cosine", "precomputed"], key="affinity")
        num_clusters = st.slider(label="Select no. of clusters", min_value=1, max_value=300, value=10, step=1, key="num_clusters")
        submit_btn = st.form_submit_button(label = "Cluster Duplicates", on_click=cluster_duplicates, args=(df, filename))

def data_uploader_form():
   with st.form(key="file_upload"):
       data_file = st.file_uploader("Upload File", type=['csv', 'xlsx'], key="upload")
       delim_list = ["|", r"\t", ",", ";"]
       delim = st.selectbox("Select File Seperator/Delimiter", delim_list, key="delim")
       submit_file_btn = st.form_submit_button(label='Profile Data', on_click=profiler)

if __name__ =="__main__":
    st.set_page_config(layout="wide")
    st.title("Slalom Data Profiler")
    st.subheader("Welcome to Q4 learning day")
    data_uploader_form()